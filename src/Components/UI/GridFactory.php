<?php
declare(strict_types=1);

namespace Mepatek\Components\UI;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\QueryBuilder;
use Nette\ComponentModel\IContainer;
use Nette\Database\Table\Selection;
use Nette\Localization\ITranslator;
use Ublaboo\DataGrid\DataGrid;
use Ublaboo\DataGrid\DataSource\ApiDataSource;
use Ublaboo\DataGrid\DataSource\ArrayDataSource;
use Ublaboo\DataGrid\DataSource\DoctrineCollectionDataSource;
use Ublaboo\DataGrid\DataSource\DoctrineDataSource;
use Ublaboo\DataGrid\DataSource\IDataSource;
use Ublaboo\DataGrid\DataSource\NetteDatabaseTableDataSource;
use Ublaboo\DataGrid\Exception\DataGridException;

/**
 * Class GridFactory
 * @package App\Components\UI
 */
class GridFactory
{
    /** @var ITranslator */
    private $translator;

    /**
     * Create grid UI component for API
     *
     * @param string $url
     * @param array $queryParams
     * @param string $primaryKey
     * @param integer|null $perPage
     * @param IContainer|null $parent
     * @param string|null $name
     *
     * @return DataGrid
     * @throws DataGridException
     */
    public function createApi(
        string $url,
        array $queryParams = [],
        ?string $primaryKey = null,
        ?int $perPage = null,
        ?IContainer $parent = null,
        ?string $name = null
    ): DataGrid {
        $dataSource = new ApiDataSource($url, $queryParams);
        return $this->create(
            $dataSource,
            $primaryKey,
            $perPage,
            $parent,
            $name
        );
    }

    /**
     * Create grid UI component (Ublaboo/DataGrid
     *
     * @param array|QueryBuilder|Selection|IDataSource $data
     * @param string $primaryKey
     * @param integer $perPage
     * @param IContainer|null $parent
     * @param string|null $name
     *
     * @return DataGrid
     * @throws DataGridException
     */
    public function create(
        $data = null,
        ?string $primaryKey = null,
        ?int $perPage = null,
        ?IContainer $parent = null,
        ?string $name = null
    ): DataGrid {
        $grid = new DataGrid($parent, $name);

        // set primary key
        if ($primaryKey) {
            $grid->setPrimaryKey($primaryKey);
        }

        // set data source
        if ($data) {
            if ($data instanceof IDataSource) {
                $dataSource = $data;
            } elseif ($data instanceof QueryBuilder) {
                $dataSource = new DoctrineDataSource($data, $primaryKey);
            } elseif ($data instanceof Collection) {
                $dataSource = new DoctrineCollectionDataSource($data, $primaryKey);
            } elseif ($data instanceof Selection) {
                $dataSource = new NetteDatabaseTableDataSource($data, $primaryKey);
            } else {
                $dataSource = new ArrayDataSource($data);
            }
        } else {
            $dataSource = new ArrayDataSource([]);
        }
        $grid->setDataSource($dataSource);

        // set properties of grid
        if ($this->translator) {
            $grid->setTranslator($this->getTranslator());
        }

        // set item per page
        if ($perPage) {
            $grid->setDefaultPerPage($perPage);
        } else {
            $grid->setPagination(false);
        }

        return $grid;
    }

    /**
     * @return ITranslator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param ITranslator $translator
     */
    public function setTranslator(ITranslator $translator)
    {
        $this->translator = $translator;
    }
}
