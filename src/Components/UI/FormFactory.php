<?php
declare(strict_types=1);

namespace Mepatek\Components\UI;

use Nette\Localization\ITranslator;

/**
 * Class FormFactory
 * @package Mepatek\Components\UI
 */
class FormFactory
{
    /** @var ITranslator */
    private $translator;

    /**
     * Create form UI component
     *
     * @return Form
     */
    public function create(): Form
    {
        $form = new Form();
        if ($this->translator) {
            $form->setTranslator($this->getTranslator());
        }
        return $form;
    }

    /**
     * @return ITranslator
     */
    public function getTranslator()
    {
        return $this->translator;
    }

    /**
     * @param ITranslator $translator
     */
    public function setTranslator(ITranslator $translator)
    {
        $this->translator = $translator;
    }
}
