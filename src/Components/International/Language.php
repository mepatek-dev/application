<?php
declare(strict_types=1);

namespace Mepatek\Components\International;

/**
 * Class Language
 * @package Mepatek\Components\International
 */
class Language
{
    /** @var string iso 2 char id */
    public $id;
    /** @var string english name */
    public $name;
    /** @var string original name */
    public $originalName;
    /** @var string czech name */
    public $czechName;

    /**
     * Language constructor.
     *
     * @param array $arrayData
     */
    public function __construct($arrayData)
    {
        list($this->id, $this->czechName, $this->originalName, $this->name) = $arrayData;
    }
}
