# mepatek/application

Mepatek Application library for [Nette Framework](https://nette.org/).


## Installation

Install UserManager using  [Composer](https://getcomposer.org/):

```sh
$ composer require mepatek/application
```
